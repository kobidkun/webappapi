<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Userpostdata extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        DB::table('user_posts')->insert([
            'name' => str_random(10),
            'email' => str_random(10) . '@gmail.com',
            'mobile' => str_random(5),
        ]);

        for ($i = 0; $i < 100; $i++) {
            DB::table('user_posts')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'mobile' => $faker->numberBetween(5),
            ]);
        }
    }
}

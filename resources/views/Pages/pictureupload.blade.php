@extends('structure')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">

    <div class="main-content">
        <!-- header-starts -->
    @include('Partials.topmenu')
    <!-- //header-ends -->
        <div id="page-wrapper">
            <div class="tab-content">
                <div class="tab-pane active" id="horizontal-form">

                    <form {{--action="{{route('store.products')}}"--}}
                          enctype="multipart/form-data" method="POST"
                          class="form-horizontal"
                    >
                        {{csrf_field()}}

                        @foreach ($flights as $rec)
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-2 control-label">Publish Date</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control1" value="{{ $rec->name}}" disabled=""
                                           id="date" placeholder="Publish Date">
                                </div>

                            </div>


                            <input type="hidden" class="form-control1" value="{{ $rec->id}}" disabled="" id="parentid"
                                   placeholder="Publish Date">

                            <div class="form-group">
                                <label for="selector1" class="col-sm-2 control-label">Select Project</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control1" value="{{ $rec->category}}" disabled=""
                                           id="project" placeholder="Projects">


                                    </select></div>
                            </div>
                        @endforeach


                        <div
                                style="width: 75%; align-content: center; align-items: center; "
                                class="dropzone position-center"
                                id="mydropzone"
                        >


                        </div>


                        <div
                                style="margin-top: 2%"
                        >

                            <button type="button" class="btn btn-primary center-block position-center">Create new
                                Project updates
                            </button>


                        </div>


                    </form>


                </div>
            </div>
            <!--body wrapper start-->
        </div>
        <!--body wrapper end-->
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.js"></script>
    <script>

    </script>
    <script type="text/javascript">

        Dropzone.options.mydropzone = {
            url: "api/uploadsimage",
            maxFilesize: 8,
            resizeWidth: "100px",
            resizeQuality: 0.2,
            dictDefaultMessage: "<span style=\"font-size: 128px;\n" +
            "     font-weight: 700;\n" +
            "      text-align: center;\n" +
            "     color: #0fb0ee;\n" +
            "     align-items: center;\n" +
            "     align-content: center;\n" +
            "\" class=\"lnr lnr-upload\"></span>" +
            "<div><span class=\"lnr lnr-layers\"></span> Drag &  drop images or " +
            "<br> " +
            " <span class=\"lnr lnr-pointer-up\"></span>" +
            " Click to select </div>",
            params: {
                project: document.getElementById('project').value,
                projectdate: document.getElementById('date').value,
                parentid: document.getElementById('parentid').value

            }


        };
    </script>

@endsection
@extends('structure')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">

    <div class="main-content">
        <!-- header-starts -->
    @include('Partials.topmenu')
    <!-- //header-ends -->
        <div id="page-wrapper">
            <div class="tab-content">
                <div class="tab-pane active" id="horizontal-form">

                    <form action="{{route('store.update')}}"
                          enctype="multipart/form-data" method="POST"
                          class="form-horizontal"
                    >
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-2 control-label">Publish Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="date" class="form-control1" id="projectdate"
                                       placeholder="Publish Date">
                            </div>

                        </div>


                        <div class="form-group">
                            <label for="selector1" class="col-sm-2 control-label">Select Project</label>
                            <div class="col-sm-8"><select name="project" id="project" class="form-control1">
                                    <option value="Embee Builders">Embee Builders.</option>
                                    <option value="Embee Delight">Embee Delight</option>
                                    <option value="Squarewood Utsab">Squarewood Utsab.</option>
                                </select></div>
                        </div>


                        <div
                                style="margin-top: 2%"
                        >

                            <button type="submit" class="btn btn-primary center-block position-center">Create new
                                Project updates
                            </button>


                        </div>


                    </form>


                </div>
            </div>
            <!--body wrapper start-->
        </div>
        <!--body wrapper end-->
    </div>


@endsection
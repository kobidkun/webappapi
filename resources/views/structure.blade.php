<!DOCTYPE HTML>
<html>
<head>
    <title>Embee Admin Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Embee Admin Template"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- Bootstrap Core CSS -->
    <link href="css/main.css" rel='stylesheet' type='text/css'/>
    <!-- //lined-icons -->
    <!-- chart -->

    <!-- //chart -->
    <!--animate-->

    <!--//end-animate-->
    <!----webfonts--->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <!-- Meters graphs -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Placed js at the end of the document so the pages load faster -->

</head>

<body class="sticky-header left-side-collapsed">
<section>
    <!-- left side start-->
@include('Partials.left')
<!-- left side end-->

    <!-- main content start-->
@yield('content')
<!--footer section start-->
    <footer>
        <p>&copy 2015 Easy Admin Panel. All Rights Reserved | Design by <a href="https://tecions.com/" target="_blank">Tecions.</a>
        </p>
    </footer>
    <!--footer section end-->

    <!-- main content end-->
</section>

<script src="js/all.js"></script>


</body>
</html>
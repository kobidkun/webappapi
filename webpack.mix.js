let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.styles([
    'public/css/bootstrap.min.css',
    'public/css/font-awesome.css',
    'public/css/icon-font.min.css',
    'public/css/css/graph.css',
    'public/css/css/animate.css',
    'public/css/style.css'
], 'public/css/main.css')
    .scripts([
        'public/js/jquery.nicescroll.js',
        'public/js/scripts.js',
        'public/js/bootstrap.min.js'
    ], 'public/js/all.js');

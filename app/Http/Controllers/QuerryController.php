<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\UserPost;

class QuerryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $querrycontrollers = UserPost::all();

        return response()->json($querrycontrollers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projectstatus.projectupdates');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postquerry = UserPost::create($request->all());
        $subject = 'New Querry from Embee Mobile';
        $time = time();
        $data = array('name' => $request->name, 'email' => $request->email, 'mobile' => $request->mobile,);
        Mail::send('mail', $data, function ($message) {
            $message->to(['soumyarayofficial@gmail.com', 'clientbuy@tecions.com']);

            $message->subject('New Querry from Embee Mobile');
            $message->from('embeemobile@embeebuilders.com', 'Embee Lite');
        });
        return response()->json($postquerry, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

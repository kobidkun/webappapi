<?php

namespace App\Http\Controllers\Manageuser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Userbase;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class Register extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'mobile' => 'required|numeric',
            'password' => 'required|min:5',
        ]);


        $userregister = new User();
        $userregister->name = $request->name;
        $userregister->lastname = $request->lastname;

        $userregister->email = $request->email;
        $userregister->mobile = $request->mobile;
        $userregister->city = $request->city;
        $userregister->state = $request->state;
        $userregister->country = $request->country;
        $password = $request->password;
        $userregister->password = bcrypt($password);
        $userregister->save();
        return response()->json([
            'message' => 'success'], 201);


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function signin(Request $request)
    {
        $credentials = $request->only('email', 'password'); // grab credentials from the request
        try {
            if (!$token = JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500); // something went wrong whilst attempting to encode the token
        }
        // $userId = $token->id;


        return response()->json(['token' => "$token"], 200);

    }

    public function getAuthUser(Request $request)
    {
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }
}

<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\ProjectStatus as Status;
use App\ProjectStatusImages;

class ProjectStatus extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createnewupdates()
    {
        return view('Pages.Homepage');
    }

    public function postnewupdates(Request $request)
    {
        $projectupdates = new Status();
        $projectupdates->name = $request->date;
        $projectupdates->category = $request->project;
        $projectupdates->save();
        $last_insert_id = $projectupdates->id;


        //   $users = DB::table('project_statuses')->where('id',  $last_insert_id)->get();

        $flights = Status::find(['id', $last_insert_id]);

        // echo $flights->name;


        return view('Pages.pictureupload', ['flights' => $flights]);

    }

    public function insertimages(Request $request)
    {
        //  return view('Pages.pictureupload');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uploadsimages = new ProjectStatusImages();
        $image = $request->file('file');
        $imageName = time() . '_' . $image->getClientOriginalName();
        $image->move(public_path('project'), $imageName);
        $imgpath = 'project/';
        $imgname = $image->getClientOriginalName();
        $removeimageextinsion = strtok($imgname, '.');
        $finalimageurl = $imgpath . $imageName;
        $replacesextraspaces = str_replace(' ', '_', $finalimageurl);


        //db save
        $uploadsimages->project = $request->project;
        $uploadsimages->parentid = $request->parentid;

        $uploadsimages->date = $request->projectdate;
        $uploadsimages->name = $removeimageextinsion;
        $uploadsimages->url = $replacesextraspaces;
        $uploadsimages->save();

        return response()->json(['success' => $imageName,]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

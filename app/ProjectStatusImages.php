<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectStatusImages extends Model
{
    protected $fillable = [
        'name'
    ];
}

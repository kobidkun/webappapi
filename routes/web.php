<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProjectStatus@createnewupdates')->name('create.update');


Route::post('/', 'ProjectStatus@postnewupdates')->name('store.update');


Route::get('insertimages', 'ProjectStatus@insertimages')->name('create.images');
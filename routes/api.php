<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('userpost', 'QuerryController@index');
Route::post('userpost', 'QuerryController@store');

Route::post('uploadsimage', 'ProjectStatus@store')->name('store.products');
Route::post('registeruser', 'Manageuser\Register@store')->name('store.user');
Route::post('auth', 'Manageuser\Register@signin')->name('signin.user');


/*Route::post('/auth',
  [
      'user' => 'Manageuser\Register@signin'

  ]  );*/

// 'Manageuser\Register@signin')

//  ->name('store.signin');

Route::get('/kkk', function (Request $request) {
    return ['name' => 'kobid'];
})->middleware('jwt.auth');




